---
title: "NetlifyおよびNetlify CMSの利用"
draft: true
---

GitLabとNetlifyを連携させて自分のWebサイトを作る．
さらにNetlify CMSを利用して，Web上で記事のポストができるようにする．

<!--more-->

## Rokko-primer-exampleを利用して作成する

1. [GitLab](https://gitlab.com)のアカウントを作成
2. GitLabにログイン
   - "New Project"で `rokko-primer-example` (他でも良い)を作成
3. Rokko-primer-exampleをcloneする
   - VS Codeを起動
   - "表示 / コマンドパレット" で "Git クローン" を実行
     - https://gitlab.com/tamura70/rokko-primer-example.git
   - コマンドパレットで "Git remove remote" を実行
     - `origin`を選択して削除
   - コマンドパレットで "Git add remote" を実行
     - `origin`を入力
     - URLに `https://gitlab.com/UUU/rokko-primer-example.git` を入力
     - `rokko-primer-example` は変更可能
   - ソース管理でプッシュを選択
     - GitLabのIDとPWを入力
   - GitLabで自分のプライベートリポジトリが作成されている
4. <https://www.netlify.com> をアクセス
   - "Sign up"で"GitLab"を選択しログインする
   - "New site from Git"を選択
   - "Continuous Development"で"GitLab"を選択
   - 自分のリポジトリの一覧が表示されるので `rokko-primer-example` を選択
   - "Deploy site"をクリック
   - "Deploys"のタブで構築状況がわかる
5. 自分のWebサイトができる!
   - `https://????.netlify.com` でサイトが公開される (https!)
6. VS Codeで自分のリポジトリを編集する
   - コンテンツは `content` フォルダ中にある
   - 自由に編集しGitLabにpushすれば，自動的に自分のサイトも更新される!
7. Netlify CMSを使って記事のポストをできるようにする
   - "Identity" で "Enable Identity"
   - "Identity / Settings and usage"
     - "Registration" を "Invite only"
     - "External providers" で "GitLab" を選択
     - "Git Gateway" で "Enable Git Gateway"
8. 自分をポストできるユーザとして登録する
   - "Identity / Settings and usage"
     - "Invite Users"をクリックし自分のメールアドレスを入力
     - 大学のメールアドレスだと届くのに時間がかかるようだ
     - 届いたメールの "Accept the invite" のURLを開く
     - "Continue with GitLab" をクリック
     - GitLabの "Authorize" をクリック
9. 記事をポストする
   - 自分のWebサイトを開く
   - URLの最後を `/admin/` に変更する
     - "New Post"をクリック
     - 記事を書く (Markdownも利用できる)
     - "Public"をクリック
   - しばらくすると新しい記事が自分のWebサイトに現れる!

上の「Rokko-primer-exampleをcloneする」手順は，コマンドラインからだと以下のようになる．

```
git clone https://gitlab.com/tamura70/rokko-primer-example.git
cd rokko-primer-example
git remote rm origin
git remote add origin master https://gitlab.com/UUU/rokko-primer-example.git
git push -u origin master
```

## Netlify CMSのデモサイトを利用して作成する

1. GitLabのアカウントを作成
2. <https://www.netlifycms.org> をアクセス
3. "GET STARTED"をクリック
4. "Hugo Site Starter"をクリック
5. "connect to GiLab"をクリック
   - GitLabでログイン
6. "Save & Deploy"をクリック
7. これでコーヒーのサイトができる
8. 上記と同じようにすればNetlify CMSでポストもできる

