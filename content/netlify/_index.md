---
title: "NetlifyおよびNetlify CMSの利用"
---

GitLabとNetlifyを連携させて自分のWebサイトを作る．
さらにNetlify CMSを利用して，Web上で記事のポストができるようにする．

<!--more-->

## このサイトのコピーを作成する

1. [GitLab](https://gitlab.com)のアカウントを作成し，ログイン
   - 右上のアイコンから "Settings" を選択
   - 左のメニューで "Preferences" を選択
   - "Localization / Language" で "日本語" を選択し "Save Changes" をクリック
2. rokko-primer-exampleをforkする
   - <https://gitlab.com/tamura70/rokko-primer-example>を開く
   - "フォーク"をクリックし自分のアカウントを選択
   - これで rokko-primer-example のコピーができる (プライベートリポジトリ)
3. <https://www.netlify.com> を開く
   - "Sign up"で"GitLab"を選択しログインする
   - "New site from Git"を選択
   - "Continuous Development"で"GitLab"を選択
   - 自分のリポジトリの一覧が表示されるので `rokko-primer-example` を選択
   - "Deploy site"をクリック
   - "Deploys"のタブで構築状況がわかる
4. 自分のWebサイトが構築される!
   - `https://????.netlify.com` でサイトが公開されている (https!)
5. GitLabで編集
   - 左のメニューから "リポジトリ / ファイル" を選択
   - `content` フォルダの `_index.md` を選択
   - "編集" ボタンで編集画面に移り，ファイルを編集
   - 画面下の "Commit Changes" ボタンを押す
   - しばらく待つと，自分のWebサイトの内容が更新される!
   - ついでに以下を行い，リポジトリの親子関係を削除しておく
     - 左のメニューで "設定" を選択
     - "高度な設定" を展開し "Remove fork relationship" をクリック
     - 確認のため "rokko-primer-example" と入力
6. Netlify CMSを使って記事のポストをできるようにする
   - Netlifyにログイン
   - "Identity" で "Enable Identity"
   - "Identity / Settings and usage"
     - "Registration" を "Invite only"
     - "External providers" で "GitLab" を選択
     - "Git Gateway" で "Enable Git Gateway"
7. 自分をポストできるユーザとして登録する
   - "Identity / Settings and usage"
     - "Invite Users"をクリックし自分のメールアドレスを入力
     - 大学のメールアドレスだと届くのに時間がかかるようだ
     - 届いたメールの "Accept the invite" のURLを開く
     - "Continue with GitLab" をクリック
     - GitLabの "Authorize" をクリック
8. 記事をポストする
   - 自分のWebサイトを開く
   - URLの最後を `/admin/` に変更する
     - "New Post"をクリック
     - 記事を書く (Markdownも利用できる)
     - "Public"をクリック
   - しばらくすると新しい記事が自分のWebサイトに現れる!

## Netlify CMSのデモサイトをコピーして作成する

1. GitLabのアカウントを作成
2. <https://www.netlifycms.org> をアクセス
3. "GET STARTED"をクリック
4. "Hugo Site Starter"をクリック
5. "connect to GiLab"をクリック
   - GitLabでログイン
6. "Save & Deploy"をクリック
7. これでコーヒーのサイトができる
8. 上記と同じようにすればNetlify CMSでポストもできる

