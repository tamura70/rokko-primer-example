---
title: "Rokko-primer設定ガイド"
---

## 前提

- hugoがインストールされていること．

## 利用方法

- このパッケージをダウンロードし，以下を実行する．

         hugo server

- Webブラウザで <http://localhost:1313> を開く．
    - Ctrl-C で停止する．
- 以下を実行すると `public` フォルダー中にHTMLファイルが生成される．

         hugo

`hugo`コマンドの使い方は`hugo help`を実行するか，以下を参照する．

- 参考: <https://gohugo.io/getting-started/usage/>

## config.tomlのカスタマイズ

- 参照: [config.tomlのカスタマイズ](config/)

