---
title: "アラート表示のテスト"
nav_alert:
  tags: [ "alert" ]
---

アラート表示のテスト

<!--more-->

front matterに以下のように記述する．

```
nav_alert:
  tags: [ "alert" ]
```

タグにalertがあるページの一覧が表示される．
表示位置は `params.navigations.alert_page` の指定による．


