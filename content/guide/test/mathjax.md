---
title: "Mathjaxのテスト"
mathjax: true
tags: [ "mathjax" ]
---

Mathjaxのテスト

<!--more-->

front matterに以下のように記述する．

```
mathjax: true
```

When <span>$a \ne 0$</span>, there are two solutions to \(ax^2 + bx + c = 0\) and they are
$$x = {-b \pm \sqrt{b^2-4ac} \over 2a}.$$

<span>$A_1 \land B_2 \Rightarrow C_1$</span>

<div>$$
\begin{align*}
  A & = B \\
  C & \ge D
\end{align*}
$$</div>


