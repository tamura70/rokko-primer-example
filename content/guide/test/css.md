---
title: "CSSのテスト"
custom_css: [ "css/custom1.css" ]
categories:
  - test
tags:
  - navigation
navigations:
  alert_pages: 0
  toc: 1
  section_toc: 1
  related: 1
  links: 1
  breadcrumb: true
  tags: true
  categories: true
---

CSSのテスト

<!--more-->

## CSS

`css/custom1.css` を利用している．

```
custom_css: [ "css/custom1.css" ]
```

また `css/custom1.css` 内で背景の画像を設定している．

## Navigations

ナビゲーションはすべて左に表示する．

```
navigations:
  alert_pages: 0
  toc: 1
  section_toc: 1
  related: 1
  links: 1
  breadcrumb: true
  tags: true
  categories: true
```

