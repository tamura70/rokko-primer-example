---
title: "Test of diable_translation"
lastmod: 2019-08-02
tags:
  - language
disable_translation: true
site_title:
  name: "Title"
  url: "/test/"
---

Test of diable_translation.

<!--more-->

ナビゲーションやメニューの表記の翻訳をオフにする．

- `menu.main`の表記には`identifier`を用いる．
- 各種ナビゲーションの見出し表記は以下に固定している．
    - `alert_page`: "Alerts"
    - `toc`: "Table of Contesnts"
    - `section_toc`: "Pages"
    - `related`: "Related Pages"
    - `links`: "Links"
- サイトのタイトルが日本語の場合はfront matterで`site_title`を設定する．




