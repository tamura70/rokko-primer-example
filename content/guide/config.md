---
title: "config.tomlのカスタマイズ"
---

`config.toml` ファイルを編集することで設定を変更できる．

<!--more-->

- 以下で「(Hugo)」と書かれている場合はHugoの標準パラメータである．
    - 参考: <https://gohugo.io/getting-started/configuration/>

`config.toml` ファイルで設定できるパラメータの一部を説明する．

## グローバル

- `baseURL`: ベースとなるURLの指定 (Hugo)
    - `baseURL = "http://localhost:1313/xxx/"` なら <http://localhost:1313/xxx/> がトップページになる．
- `theme`: テーマ名の指定 (Hugo)
    - `theme = "rokko-primer"` なら `themes/rokko-primer/` のテーマが利用される．
    - `theme = [ "mytheme", "rokko-primer" ]` なら最初に `themes/mytheme/` のテーマが利用され
      存在しないファイルについては `themes/rokko-primer/` のテーマが利用される．
- `pygmentsCodeFences`, `pygmentsUseClasses`: シンタックス・ハイライトの指定 (Hugo)
    - 参照: [シンタックス・ハイライト](syntax_highlight)
- `disablePathToLower`: パスを小文字に統一しない (Hugo)
    - デフォールトはfalseなので，パズ`/ABC/`とパス`/abc/`は区別されない．
- `paginate`: タグやカテゴリーの一覧表示などで表示される項目の最大数 (Hugo)
    - デフォールトは10
- `DefaultContentLanguage`: 標準の言語 (Hugo)
    - "ja"と指定した場合，`_index.md`, `xxx.md`などのMarkdownファイルは日本語として処理される．
    - 英語として処理したい場合はファイル名を `_index.en.md`, `xxx.en.md`などとする．
- `ignoreFiles`: 変換の対象としないファイルの正規表現 (Hugo)
- `rssLimit`: RSSの最大項目数 (Hugo)

## outputs

出力する形式の指定 (Hugo)．

- 参考: <https://gohugo.io/templates/output-formats/#output-formats-for-pages>

## blackfriday

MarkdownをHTMLへ変換するレンダリングエンジンの設定．

- `smartypants`: 引用符，ダッシュ，分数などの自動フォーマットを有効にする (Hugo)
    - デフォールトはtrue
    - 例: "quote", --, ---, 1/2

## languages

言語の種類と，それぞれでのパラメータの設定．

- `languages.ja`: 日本語 (Hugo)
    - `languageCode`: RSSファイル (`index.xml`)中の言語コード (Hugo)
    - `title`: サイトのタイトル．HTMLの`<title>`要素に利用される (Rokko-primer)
    - `footer`: サイトのフッター．HTMLの`<footer>`要素に利用される (Rokko-primer)
    - `hasCJKLanguage`: trueならサマリー作成時に日本語対応する (Hugo)
    - `summaryLength`: サマリーの長さ．デフォールトは70 (Hugo)
    - `pluralizeListTitles`: `title`指定がないセクション・ページでタイトルを自動生成する場合に複数形を用いる (Hugo)
- `languages.en`: 英語 (Hugo)
    - 日本語と同様

- 参考: <https://gohugo.io/content-management/multilingual/>

## languages.*.menu.main

ページの上部右に表示されるメニューの設定．

- `identifier`: ID (Hugo)
    - `params.disable_translation` が指定されている場合，これを表示する (Rokko-primer)
- `name`: メニュー名 (Hugo)
- `url`: メニューのURL (Hugo)
- `weight`: メニューの表示順序．小さい値が先になる (Hugo)
    - 指定しなければデフォールトの順序
    - 参考: <https://gohugo.io/templates/lists/#order-content>

- 参考: <https://gohugo.io/content-management/menus/>
- 参考: <https://gohugo.io/variables/menus/>
- 参考: <https://gohugo.io/templates/menu-templates/>

## languages.*.menu.links (Rokko-primer)

標準ではページに右側に表示されるリンクの設定．

- `name`: リンク名
- `url`: リンクのURL
- `weight`: リンクの表示順序．小さい値が先になる．

## params

- `netlify_cms`: Netlify CMSを有効にする (Rokko-primer)
- `description`: HTMLの`<meta name="description">`要素に利用される (Rokko-primer)
    - 各ページのfront matterで`description`が指定されていれば，それも加えられる．
- `lastmodFormat`: 各ページの上部に表示する最終更新日のフォーマット (Rokko-primer)
    - 空文字列なら表示しない
- `enable_list`: 各ページの下部にページ一覧を表示する (Rokko-primer)
    - 各ページのfront matterで指定されていれば，それが優先される．
    - どのような一覧を表示するかは`params.list`で指定する．
    - 一覧のスタイルは`params.list_style`で指定する．
- `list_class`: ページ一覧のCSSクラス名を指定する (Rokko-primer)
    - `card`ならカード表示
- `debug_show_variables`: デバッグ用に各ページでHugo変数の値を表示する (Rokko-primer)
    - 各ページのfront matterで指定されていれば，それが優先される．

## params.navigations (Rokko-primer)

- 参照: [ナビゲーション](../navigations/)

## related (Hugo)

関連ページの設定．

- 参考: <https://gohugo.io/content-management/related/>

## params.list (Rokko-primer)

各ページの下部に表示するページ一覧の設定．
これらは，各ページのfront matterで指定されていれば，それが優先される．
ただし，その場合は基本的にはすべてを設定する必要がある．
設定していないときは空文字列あるいはfalseあるいは空リストと解釈される．

- `header`: 見出し
    - 空文字列を指定すれば見出しは表示されない
    - 何も指定しなれば，日本語の場合は"ページ一覧"，英語の場合は"List of Pages"と表示
    - **注意**: この見出しは目次には現れない
- `section`: どのセクションに属するページを一覧表示するかの設定
    - `section: ".."` とすれば親セクションが対象になる
    - 指定しなれば現在のセクション
- `pages`: 一覧表示するページの種類
    - `pages: [ "sections" ]`ならセクション・ページを表示
    - `pages: [ "pages" ]`ならシングル・ページを表示
    - `pages: [ "sections", "pages" ]`なら両方を表示
    - 空なら両方を表示
- `tags`: 空でなければ，それらのタグをすべて持つページを表示する
- `categories`: 空でなければ，それらのカテゴリーをすべて持つページを表示する
- `sortby`: ソート方法の指定
    - `sortby: title`ならタイトル順でソート
    - 指定しなければデフォールトの順序
    - 参考: <https://gohugo.io/templates/lists/#order-content>

## params.list_style (Rokko-primer)

各ページの下部に表示するページ一覧の表示方法の設定．
これらは，各ページのfront matterで指定されていれば，それが優先される．
ただし，その場合は基本的にはすべてを設定する必要がある．
設定していないときは空文字列あるいはfalseあるいは空リストと解釈される．

- `section_mark`: セクション・ページの前に表示するマーク
- `lastmodFormat`: 最終更新日のフォーマット
    - 例: 2019年1月31日の場合
        - `lastmodFormat: "2006-01-02"`なら "2019-01-31" と表示
        - `lastmodFormat: "2006-01-02 (Mon)"`なら "2019-01-31 (Thu)" と表示
    - 参考: <https://gohugo.io/functions/format/#hugo-date-and-time-templating-reference>
- `pages_count`: その下にあるページの数を表示
- `tags`: タグを表示
- `categories`: カテゴリーを表示
- `summary`: サマリーを表示

## その他

- Gitリポジトリのファイルの場合，最終更新日はGitの情報が利用される
   - 参考: <https://gohugo.io/getting-started/configuration/#configure-dates>
