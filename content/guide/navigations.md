---
title: ナビゲーション
tags: [ "navigation" ]
---

`params.navigations`で各種ナビゲーション情報をページのどの位置に配置するかを指定できる．

<!--more-->

なお，タイトル直下のパンくずリンク，タグ，カテゴリーについての設定項目は存在しない．
表示を変更したければCSSで対応する．

## パラメータ

- `alert_page`: 注意のページの位置
- `toc`: 目次の位置
- `section_toc`: そのページと同レベルにあるページの一覧の位置
    - 表示する最大数は `params.section_toc` 中に指定する．
    - セクション・ページの前に表示するマークは `params.section_toc` 中に指定する．
- `related`: 関連ページの位置
    - 表示する最大数は `params.related` 中に指定する．
    - 関連ページの設定は `related` 中に記述する
- `links`: リンクの位置
    - 表示する項目は `languages.*.menu.links` で指定する
- **注意**: 同じ位置に配置したナビゲーション情報の表示順序は固定されている

値と位置の対応は以下のとおり．

| 値 | 位置 |
|---:|:---:|
| 0 | 表示しない |
| 1 | 左 |
| 2 | 右 |
| 3 | 上 |
| 4 | 下 |

特定のページで設定を変更したければ，front matterに記述すれば良い (yaml)．
ただし，その場合は基本的には同一パラメータ内のすべてを設定する必要がある．
設定していないときは0あるいは空文字列と解釈される．

   ```yaml
    navigations:
      alert_pages: 2
      toc: 2
      section_toc: 2
      related: 2
      links: 2
    section_toc:
      max: 10
      section_mark: ""
    related:
      max: 10  
    ```


## alert_page

サイト内の全ページのうち `params.nav_alert.tags` で指定したタグを持つページがあれば注意のナビゲーションとして表示する．

- `config.toml`内なら以下のように記述する．
    ```toml
    [params.nav_alert]
      tags = [ "alert" ]
    ```
- 特定のページで表示したければfront matterに以下のように記述する (yaml)．
    ```yaml
    nav_alert:
      tags: [ "alert" ]
    ```
- 上記のようにすると `alert` のタグがついたページの一覧が表示される．
- テンプレート・ファイル: `layout/partials/nav_alert_pages.html`

## toc

目次を表示する．

- 表示する目次の深さを指定するにはCSSを設定する必要がある (以下の例は`<h2>`まで表示)．
    ```css
    #TableOfContents > ul > ul > ul {
      display: none;
    }
    ```
- テンプレート・ファイル: `layout/partials/nav_toc.html`

## section_toc

- 同一レベルにあるセクション・ページとシングル・ページの一覧が表示される．
- `params.section_toc.max` が設定されていれば，その数まで表示する．
- 表示する順序はHugoのデフォールトにしたがう．
    - 指定したければ各ページに`weight`を設定する．
- セクション・ページの前には `params.section_toc.section_mark` が表示される．
- 特定のページで変更したければfront matterに以下のように記述する (yaml)．
    ```yaml
    section_toc:
      max: 10
      section_mark: "&#x2BC8; "
    ```
- テンプレート・ファイル: `layout/partials/nav_section_toc.html`

## related

- Hugoの[関連ページ](https://gohugo.io/content-management/related/)を表示する．
- `params.related.max` が設定されていれば，その数まで表示する．
- テンプレート・ファイル: `layout/partials/nav_related.html`

## links

- 表示する項目は `languages.*.menu.links` で指定する
- テンプレート・ファイル: `layout/partials/nav_links.html`

