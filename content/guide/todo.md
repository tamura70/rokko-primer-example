---
draft: true
---
- [ ] debugセクションを作る
- [ ] sectionのtitleやdescriptionを継承する
- [ ] listでimageを含めることはできるか
- [ ] 言語メニューの表示

- [x] site logoのテスト
- [x] listでmaxの指定
- [x] listでclassの指定
    - card classを作る
- [x] breadcrumb, tags, categories, lastmodの表示オプションをつける
    - `navigations.breadcrumb = 1` など
- [x] disable_translation のヘッダ表示
    - site_title があればそれを表示
- [x] debug_show_variables で .Site.Params の表示
- [x] tagの個数の表示
- [x] CSSのテスト
- [x] Netlify, Netlify CMS連携
