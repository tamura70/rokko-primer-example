# Rokko-primer example

Rokko-primer is a theme of [Hugo](https://gohugo.io) static site generator.

See the demo site at [Netlify](https://www.netlify.com).

- <https://rokko-primer.netlify.com>

